using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GUIGameplay : MonoBehaviour
{

    /** ======= MARK: - Fields and Properties ======= */

    private List<EventListener> _eventListeners;

    [SerializeField]
    private TextMeshProUGUI timerLabel;

    [SerializeField]
    private TextMeshProUGUI stageCompletedLabel;

    [SerializeField]
    private TextMeshProUGUI currentStageLabel;

    float currentTime = 30f;

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(UpdateTimer());
        AddListeners();

        stageCompletedLabel.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private bool isTimerToZero = new bool();

    IEnumerator UpdateTimer()
    {
        isTimerToZero = false;

        while (!isTimerToZero)
        {
            UpdateTimerLabel();
            yield return null;
        }
    }

    public void UpdateTimerLabel()
    {
        currentTime -= Time.deltaTime;
        if (currentTime <= 0f)
        {
            isTimerToZero = true;
            return;
        }
        TimeSpan testtime = TimeSpan.FromSeconds(currentTime);
        timerLabel.text = testtime.ToString("ss'.'ff");
    }

    /** ======= MARK: - Event Listeners ======= */

    private void AddListeners()
    {
        _eventListeners = new List<EventListener>
        {
            CustomEventSystem.instance.AddListener(EventCode.ON_BOSS_DEFEATED, this, HandleOnBossDefeatedUI)
        };
    }

    private void RemoveListeners()
    {
        if (_eventListeners.Count != 0)
        {
            foreach (EventListener listener in _eventListeners)
                CustomEventSystem.instance.RemoveListener(listener.eventCode, listener);
        }
    }

    /** ======= MARK: - Actions ======= */

    private void HandleOnBossDefeatedUI(object[] eventParam)
    {
        stageCompletedLabel.gameObject.SetActive(true);
    }

    public void OnBossTimerButtonTap()
    {
        currentTime = 30f;
        timerLabel.text = "30.00";
        StartCoroutine(UpdateTimer());
    }

    public void OnNextStageButtonTap()
    {
        if (int.Parse(GameFlowManager.instance.currentStage) + 1 == GameFlowManager.instance.listStageData.Count)
        {
            currentStageLabel.text = "Gone to end of JSON";
        } else
        {
            stageCompletedLabel.gameObject.SetActive(false);
            GameFlowManager.instance.currentStage = (float.Parse(GameFlowManager.instance.currentStage) + 1f).ToString();
            currentStageLabel.text = "Current Stage: " + GameFlowManager.instance.currentStage;
            GameFlowManager.instance.currentStageData = GameFlowManager.instance.listStageData[int.Parse(GameFlowManager.instance.currentStage)];

            CustomEventSystem.instance.DispatchEvent(EventCode.ON_NEXT_STAGE, new object[] {
            GameFlowManager.instance.currentStage
        });
        }
    }
}
