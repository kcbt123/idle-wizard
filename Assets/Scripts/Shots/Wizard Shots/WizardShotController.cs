using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WizardShotController : MonoBehaviour
{
    /** ======= MARK: - Fields and Properties ======= */

    private List<EventListener> _eventListeners;
    private Vector3 movePointTest;

    [SerializeField]
    private GameObject shotContactObject;

    [SerializeField]
    private float moveSpeed = 2000f;

    [SerializeField]
    private float shotDamage = 5f;

    /** ======= MARK: - MonoBehavior Functions ======= */

    void Start()
    {
        //AddListeners();
        movePointTest = transform.TransformPoint(0.0f, 500.0f, 0.0f);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 targetedPosition = GameFlowManager.instance.targetShotPoint.transform.position;
        float flyStep = moveSpeed * Time.deltaTime;
        if (Vector3.Distance(transform.position, targetedPosition) > flyStep)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetedPosition, flyStep);
        } else
        {
            Debug.Log("Explosion Sound");
            SoundManager.instance.wizardShotContactSFXSource.PlayOneShot(SoundManager.instance.soundList.wizardShotContactSFX);
            transform.gameObject.SetActive(false);
            shotContactObject.SetActive(true);

            CustomEventSystem.instance.DispatchEvent(EventCode.ON_WIZARD_SHOT_CONTACT, new object[] {
                shotDamage
            });
        }
    }

    /** ======= MARK: - Event Listeners ======= */

    private void AddListeners()
    {
        _eventListeners = new List<EventListener>
        {
            CustomEventSystem.instance.AddListener(EventCode.ON_TAP_WIZARD_SHOT, this, OnTapWizardShot)
        };
    }

    private void RemoveListeners()
    {
        if (_eventListeners.Count != 0)
        {
            foreach (EventListener listener in _eventListeners)
                CustomEventSystem.instance.RemoveListener(listener.eventCode, listener);
        }
    }

    /** ======= MARK: - Actions ======= */
    private void OnTapWizardShot(object[] eventParam)
    {
        float _bulletIndex = (float)eventParam[0];
        transform.position = Vector3.MoveTowards(transform.position, movePointTest, moveSpeed * Time.deltaTime);
    }

    /** ======= MARK: - Triggers ======= */

    private void OnCollisionEnter(Collision other)
    {
        Debug.Log("Bullet Side: " + other.gameObject);
    }
}
