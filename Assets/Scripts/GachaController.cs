using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GachaController : MonoBehaviour
{
    string[,] roll10Poll = new string[10, 2];

    string[,] gachaPool = new string[,] {
        { "Slime", "R" },
        { "Bird", "SR" },
        { "Eagle", "R" },
        { "Dragon", "SR" },
        { "Dog", "SR" },
        { "Cat", "R" },
        { "Mouse", "R" },
        { "Tiger", "SSR" },
        { "Lion", "SSR" },
        { "Chimera", "SR" },
        { "Wolf", "SR" },
        { "Fox", "SR" },
        { "Crow", "SR" },
        { "Parrot", "R" },
        { "Whale", "SSR" },
        { "Shark", "R" },
        { "Ant", "R" },
        { "Fly", "R" },
        { "Falcon", "SR" }
    };

    public void RollBatch10()
    {
        roll10Poll = new string[10, 2];
        int amount = 10;

        for (int i = 0; i < amount; i++)
        {
            int rarityPercent = Random.Range(0, 100);

            string rolledRarity = GetClosestRarity(rarityPercent);

            int totalMatchedItems = 0;
            string[,] matchedRarityPool = new string[10, 2];
            int matchedRarityPoolCounter = 0;
            for (int j = 0; j < gachaPool.Length / 2; j++)
            {
                if (gachaPool[j, 1] == rolledRarity)
                {
                    totalMatchedItems++;
                    matchedRarityPool[matchedRarityPoolCounter, 0] = gachaPool[j, 0];
                    matchedRarityPool[matchedRarityPoolCounter, 1] = gachaPool[j, 1];
                    matchedRarityPoolCounter++;
                }
            }

            if (totalMatchedItems <= 0)
            {
                return;
            }

            // Get Random item
            int randomIndex = Random.Range(0, totalMatchedItems - 1);

            roll10Poll[i, 0] = matchedRarityPool[randomIndex, 0];
            roll10Poll[i, 1] = matchedRarityPool[randomIndex, 1];
        }

        Debug.Log(roll10Poll);
        for (int i = 0; i < roll10Poll.Length / 2; i++)
        {
            Debug.Log("==== Rolled unit name: " + roll10Poll[i, 0] + ", rarity: " + roll10Poll[i, 1]);
        }
    }

    private string GetClosestRarity(int percentage)
    {
        // Percentage la cai roll ra duoc
        Debug.Log("==== Percentage rolled: " + percentage);
        string[,] rarityList = new string[,] { 
            { "R", "80" }, 
            { "SR", "10" },
            { "SSR", "10" }
        };

        int currentPercentage = 0;

        for (int i = 0; i < rarityList.Length; i++)
        {
            currentPercentage += int.Parse(rarityList[i, 1]);
            if (currentPercentage >= percentage)
            {
                return rarityList[i, 0];
            }
        }

        return "R";
    }
}