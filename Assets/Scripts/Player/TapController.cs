using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapController : MonoBehaviour
{
    public GameObject bulletContainer;
    public GameObject shotInitEffectObject;

    private void Start()
    {
        for (int i = 0; i < bulletContainer.transform.childCount; i++)
        {
            Transform bullet = bulletContainer.transform.GetChild(i);
            bullet.gameObject.SetActive(false);
        }

        shotInitEffectObject.SetActive(false);
    }

    private void Update()
    {
        Animator animator = shotInitEffectObject.GetComponent<Animator>();
        if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !animator.IsInTransition(0))
        {
            Debug.Log("Finish init effect");
            shotInitEffectObject.SetActive(false);
        }

        if (Input.GetButtonDown("Fire1"))
        {
            // - HieuBT: Get last active shot index
            int lastActiveShotIndex = -1;
            for (int i = 0; i < bulletContainer.transform.childCount; i++)
            {
                if (bulletContainer.transform.GetChild(i).gameObject.activeSelf == false)
                {
                    lastActiveShotIndex = i - 1;
                    break;
                }
            }

            if ((lastActiveShotIndex == -1 && bulletContainer.transform.GetChild(0).gameObject.activeSelf == false) || lastActiveShotIndex != -1)
            {
                // - HieuBT: Activate Shot Init Effect at current position
                shotInitEffectObject.SetActive(true);
                shotInitEffectObject.transform.position = Input.mousePosition;

                SoundManager.instance.wizardShotSFXSource.PlayOneShot(SoundManager.instance.soundList.basicWizardShotSFX);
                GameObject bullet = bulletContainer.transform.GetChild(lastActiveShotIndex + 1).gameObject;
                bullet.SetActive(true);
                bullet.transform.position = Input.mousePosition;
            }
        }
    }
}
