using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillTreeController : MonoBehaviour
{
    // Start is called before the first frame update
    public static int  SkillPoint;
    public static int UnlockPos;
    public static List<int> unlockedNodeList;
    public static GameObject notice;
    public static GameObject skillDescription;

    void Start()
    {        
        unlockedNodeList = new List<int>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetupSkillNode(Transform Node){ 
        if(unlockedNodeList.Contains(Node.GetComponent<SkillTreeNode>().nodeIndex)){
            Node.GetComponent<SkillTreeNode>().UnLockNode();
        }
        for(int i=0;i<Node.GetChild(1).childCount;i++){
            SetupSkillNode(Node.GetChild(1).transform.GetChild(i).transform);
        }    
    }
    public void Unlock(){
        if(!unlockedNodeList.Contains(UnlockPos)){
            unlockedNodeList.Add(UnlockPos);
        }
    }
}
