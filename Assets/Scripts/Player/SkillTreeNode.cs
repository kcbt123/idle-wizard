using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillTreeNode : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    public int nodeIndex;
    public bool Unlock = false;

    public void ClickNode(){
        if(Unlock){
            ShowInfo();
            return;
        }
        if(!transform.parent.transform.parent.GetComponent<SkillTreeNode>().Unlock){
            Debug.Log("Unlock parent node first!!");
            return;
        }
        SkillTreeController.UnlockPos = nodeIndex;
        GameObject unlockNotice =  GameObject.Find("SkillTreeNotices").transform.GetChild(0).gameObject;
        unlockNotice.SetActive(true);
        unlockNotice.transform.GetChild(2).GetComponent<Text>().text = "Unlock: "+nodeIndex.ToString();
    }
    public void UnLockNode(){
        this.Unlock=true;
        transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(false);
    }
    public void ShowInfo(){
        GameObject skillInfo =  GameObject.Find("SkillTreeNotices").transform.GetChild(1).gameObject;
        skillInfo.SetActive(true);
        skillInfo.transform.GetChild(2).GetComponent<Text>().text = "Info: "+nodeIndex.ToString();
    }
}
