using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class FamilliarDismantleController : MonoBehaviour
{
    // Start is called before the first frame update
    public static int TotalSelected=0;
    private GameObject Inventory;
    public GameObject Prefab;
    private int LineCount;
    void Start()
    {
        Inventory = transform.gameObject;
        LineCount = (int)Math.Ceiling((double)transform.childCount / 7);
        transform.GetComponent<RectTransform>().sizeDelta = new Vector2(812, 110 * LineCount);
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void AddFamilliar()
    {
        var copy = Instantiate(Prefab);
        copy.transform.parent = transform;
    }
}
