using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FamilliarDismantleSlot : MonoBehaviour ,IPointerClickHandler
{
    public bool Selected = false;
    public Image color;

    // Start is called before the first frame update
    void Start()
    {
        if(Selected){
            Debug.Log(transform.GetChild(0));
            Debug.Log(transform.GetChild(0).gameObject);
            color.color = new Color32(100,100,255,255) ;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnPointerClick(PointerEventData eventData)
    {
        Selected = !Selected;
        if(Selected){
            FamilliarDismantleController.TotalSelected++;
            color.color = new Color32(100,100,255,255) ;
        }else {
            color.color = new Color32(255,255,255,255) ;
            FamilliarDismantleController.TotalSelected++;
        }
    }
}
