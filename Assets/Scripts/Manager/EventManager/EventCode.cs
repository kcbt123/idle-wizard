using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventCode
{
    public static string ON_OPEN_GUI = "ON_OPEN_GUI";
    public static string ON_CLOSE_GUI = "ON_CLOSE_GUI";

    public static string ON_BOSS_DEFEATED = "ON_BOSS_DEFEATED";
    public static string ON_NEXT_STAGE = "ON_NEXT_STAGE";

    public static string ON_TAP_WIZARD_SHOT = "ON_TAP_WIZARD_SHOT";
    public static string ON_WIZARD_SHOT_CONTACT = "ON_WIZARD_SHOT_CONTACT";
}
