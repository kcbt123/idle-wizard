using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;

public class GameFlowManager : MonoBehaviour
{
    /** ======= MARK: - Init ======= */
    public string GetClassName()
    {
        return this.GetType().Name;
    }

    private GameFlowManager()
    {
        if (_instance == null)
            _instance = this;
    }

    /** ======= MARK: - Singleton ======= */

    private static GameFlowManager _instance;

    public static GameFlowManager instance
    {
        get
        {
            return _instance;
        }
    }

    // ========== MARK: - Fields and properties ==========


    private List<EventListener> _eventListeners;

    public GameObject targetShotPoint;

    [SerializeField]
    private GameObject shotContactObject;

    public List<StageData> listStageData;

    public StageData currentStageData;

    public string currentStage = "1";

    // ========== MARK: - MonoBehaviour Methods ==========

    private void Awake()
    {
        AddListeners();

        LoadJSONData();
        //SetStageInfo();
    }

    private void Start()
    {
        SoundManager.instance.backgroundMusicSource.clip = SoundManager.instance.soundList.stageTheme;
        SoundManager.instance.backgroundMusicSource.Play();

        shotContactObject.SetActive(false);
    }

    private void Update()
    {
        Animator animator = shotContactObject.GetComponent<Animator>();
        if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !animator.IsInTransition(0))
        {
            Debug.Log("Finish contact effect");
            shotContactObject.SetActive(false);
        }
    }

    private void OnDestroy()
    {
        //RemoveListeners();
    }

    /** ======= MARK: - Handle Events ======= */

    private void AddListeners()
    {
        _eventListeners = new List<EventListener>
        {

        };
    }

    private void RemoveListeners()
    {
        if (_eventListeners.Count != 0)
        {
            foreach (EventListener listener in _eventListeners)
                CustomEventSystem.instance.RemoveListener(listener.eventCode, listener);
        }
    }

    private void LoadJSONData()
    {
        listStageData = JSONUtil.LoadDataFromJson<StageDataParent>("StageData").stageData;
        currentStageData = GameFlowManager.instance.listStageData[1];
    }

    public GameObject bulletContainer;
    private bool autoShotActivatedFlag = new bool();
    public void TriggerAutoShot()
    {
        StartCoroutine(AutoShot());
    }

    IEnumerator AutoShot()
    {
        autoShotActivatedFlag = true;

        while (autoShotActivatedFlag)
        {
            int lastActiveShotIndex = -1;
            for (int i = 0; i < bulletContainer.transform.childCount; i++)
            {
                if (bulletContainer.transform.GetChild(i).gameObject.activeSelf == false)
                {
                    lastActiveShotIndex = i - 1;
                    break;
                }
            }

            if ((lastActiveShotIndex == -1 && bulletContainer.transform.GetChild(0).gameObject.activeSelf == false) || lastActiveShotIndex != -1)
            {
                SoundManager.instance.wizardShotSFXSource.PlayOneShot(SoundManager.instance.soundList.basicWizardShotSFX);
                GameObject bullet = bulletContainer.transform.GetChild(lastActiveShotIndex + 1).gameObject;
                bullet.SetActive(true);
                bullet.transform.position = new Vector3(100.0f, 500.0f, 0.0f);
            }

            yield return new WaitForSeconds(0.1f);
        }
    }
}
