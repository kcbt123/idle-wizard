using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundList
{
    public AudioClip buttonSFX;
    public AudioClip basicWizardShotSFX;
    public AudioClip wizardShotContactSFX;

    public AudioClip stageTheme;
}