using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StageDataParent
{
    public List<StageData> stageData;
}

[System.Serializable]
public class StageData
{
    public int stageID;
    public string monsterID;
    public string monsterType;
    public string monsterHP;
    public Reward reward;
}

[System.Serializable]
public class Reward
{
    public string gold;
    public string ruby;
    public string sapphire;
    public string emerald;
    public string diamond;
    public ArtifactReward artifacts;
}

[System.Serializable]
public class ArtifactReward
{

}
