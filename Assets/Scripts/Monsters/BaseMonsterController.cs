using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BaseMonsterController : MonoBehaviour
{
    /** ======= MARK: - Fields and Properties ======= */

    private List<EventListener> _eventListeners;

    private float monsterHP = 0f;

    [SerializeField]
    private TextMeshProUGUI monsterHPLabel;

    /** ======= MARK: - MonoBehavior Functions ======= */

    void Start()
    {
        AddListeners();
        monsterHP = float.Parse(GameFlowManager.instance.currentStageData.monsterHP);
        monsterHPLabel.text = monsterHP.ToString();
    }

    // Update is called once per frame
    void Update()
    {

    }

    /** ======= MARK: - Event Listeners ======= */

    private void AddListeners()
    {
        _eventListeners = new List<EventListener>
        {
            CustomEventSystem.instance.AddListener(EventCode.ON_WIZARD_SHOT_CONTACT, this, OnWizardShotContact),
            CustomEventSystem.instance.AddListener(EventCode.ON_NEXT_STAGE, this, OnNextStage)
        };
    }

    private void RemoveListeners()
    {
        if (_eventListeners.Count != 0)
        {
            foreach (EventListener listener in _eventListeners)
                CustomEventSystem.instance.RemoveListener(listener.eventCode, listener);
        }
    }

    /** ======= MARK: - Actions ======= */
    private void OnWizardShotContact(object[] eventParam)
    {
        float shotDamage = (float)eventParam[0];

        if (monsterHP - shotDamage >= 0f)
        {
            monsterHP -= shotDamage;
            monsterHPLabel.text = monsterHP.ToString();
        } else
        {
            CustomEventSystem.instance.DispatchEvent(EventCode.ON_BOSS_DEFEATED, new object[] {

            });
        }
    }

    private void OnNextStage(object[] eventParam)
    {
        string currentStage = (string)eventParam[0];
        monsterHP = float.Parse(GameFlowManager.instance.currentStageData.monsterHP);
        monsterHPLabel.text = monsterHP.ToString();
    }
}
