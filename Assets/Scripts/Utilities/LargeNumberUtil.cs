using System.Collections;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System.Text.RegularExpressions;
[CreateAssetMenu(fileName = "LargeNumberUtil", menuName = "Component/LargeNumber", order = 0)]
public class LargeNumberUtil : ScriptableObject
{
    public string TestNumber;
    public string TestNumber2;
    public string Addtion(string Number, string AddedNumber)
    {
        if (!CompareNumber(Number, AddedNumber))
        {
            return Addtion(AddedNumber, Number);
        }
        float fNumber = 0;
        float fAddedNumber = 0;
        ExtractStringNumber(ref Number, ref fNumber);
        ExtractStringNumber(ref AddedNumber, ref fAddedNumber);
        Debug.Log(Number);
        Debug.Log(AddedNumber);
        if (Number == AddedNumber)
        {
            Debug.Log(1);
            fNumber += fAddedNumber;
            while (fNumber > 1000)
            {
                fNumber = fNumber / 1000;
                if (Number == "") return fNumber.ToString("F2") + " A";
                if (Number.Length == 1)
                {
                    if (Number[0] == 'Z')
                    {
                        Number = "AA";
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(Number);
                        sb[0]++;
                        Number = sb.ToString();
                    }
                }
                else
                {
                    if (Number[1] == 'Z')
                    {
                        StringBuilder sb = new StringBuilder(Number);
                        sb[0]++;
                        sb[1] = 'A';
                        Number = sb.ToString();
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(Number);
                        sb[1]++;
                        Number = sb.ToString();
                    }
                }
            }
        }
        return fNumber.ToString("F2") + " " + Number;
    }

    public string Subtraction(string Number, string SubtractedNumber)
    {
        if (!CompareNumber(Number, SubtractedNumber))
        {
            return "";
        }
        float fNumber = 0;
        float fSubtractedNumber = 0;
        ExtractStringNumber(ref Number, ref fNumber);
        ExtractStringNumber(ref SubtractedNumber, ref fSubtractedNumber);
        if (Number == SubtractedNumber)
        {
            fNumber -= fSubtractedNumber;
            if (Number.Length == 1)
            {
                if (fNumber < 1)
                {
                    fNumber = fNumber * 1000;
                    if (Number[0] == 'A')
                    {
                        Number = "";
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(Number);
                        sb[0]--;
                        Number = sb.ToString();
                    }
                }
            }
            if (Number.Length == 2)
            {
                if (fNumber < 1)
                {
                    fNumber = fNumber * 1000;
                    if (Number[1] == 'A')
                    {
                        if (Number[0] == 'A')
                        {
                            Number = "Z";
                        }
                        else
                        {
                            StringBuilder sb = new StringBuilder(Number);
                            sb[0]--;
                            sb[1] = 'Z';
                            Number = sb.ToString();
                        }
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(Number);
                        sb[1]--;
                        Number = sb.ToString();
                    }
                }
            }

        }
        return fNumber.ToString("F2") + " " + Number;
    }

    public string Multiplication(string Number, float Multiplier)
    {
        float fNumber = 0;
        ExtractStringNumber(ref Number, ref fNumber);
        fNumber *= Multiplier;
        while (fNumber > 1000)
        {
            fNumber = fNumber / 1000;
            if (Number == "")
            {
                Number = "A";
                continue;
            }
            if (Number.Length == 1)
            {
                if (Number[0] == 'Z')
                {
                    Number = "AA";
                }
                else
                {
                    StringBuilder sb = new StringBuilder(Number);
                    sb[0]++;
                    Number = sb.ToString();
                }
            }
            else
            {
                if (Number[1] == 'Z')
                {
                    StringBuilder sb = new StringBuilder(Number);
                    sb[0]++;
                    sb[1] = 'A';
                    Number = sb.ToString();
                }
                else
                {
                    StringBuilder sb = new StringBuilder(Number);
                    sb[1]++;
                    Number = sb.ToString();
                }
            }
        }
        return fNumber.ToString("F2") + " " + Number;
    }
    //Check if Number >= ComparedNumber
    public bool CompareNumber(string Number, string ComparedNumber)
    {
        float fNumber = 0;
        float fComparedNumber = 0;
        ExtractStringNumber(ref Number, ref fNumber);
        ExtractStringNumber(ref ComparedNumber, ref fComparedNumber);
        if (Number == "" && ComparedNumber == "")
        {
            if (fNumber > fComparedNumber || fNumber == fComparedNumber) { return true; } else return false;
        }
        if (Number.Length > ComparedNumber.Length)
        {
            return true;
        }
        if (Number.Length == ComparedNumber.Length)
        {
            if (Number[0] > ComparedNumber[0])
            {
                return true;
            }
            if (Number[0] == ComparedNumber[0])
            {
                if (Number.Length == 2)
                {
                    if (Number[1] > ComparedNumber[1]) return true;
                    if (Number[1] < ComparedNumber[1]) return false;
                }
                if (fNumber > fComparedNumber || fNumber == fComparedNumber) { return true; } else return false;
            }
            else return false;
        }
        else { return false; }
    }
    //Extract string Number into float + string Numbertail(e.g AZ)
    public void ExtractStringNumber(ref string Number, ref float fNumber)
    {
        for (int i = Number.Length - 1; i > 0; i--)
        {
            if (!Char.IsDigit(Number, i))
            {
                continue;
            }
            else
            {
                fNumber = float.Parse(Number.Substring(0, i + 1));
                if (i == (Number.Length - 1))
                {
                    Number = "";
                }
                else Number = Number.Substring(i + 2);
                break;
            }
        }
    }
    //Convert from 1xx.xx to 0.1x A etc
 public string ConvertNumber(string NumberConvert)
    {
        string Number = NumberConvert;
        bool NumConverted = false;
        float NumberHolder = 0;
        ExtractStringNumber(ref Number, ref NumberHolder);
        while (NumberHolder > 10)
        {
            if(Number=="ZZ"){
            Debug.Log("Max number can't convert anymore");
            break;
        }
            NumConverted =true;
            NumberHolder /= 1000;
            if (Number == "") {
                Number="A";
                continue;
            } 
            if (Number.Length == 1)
            {
                if (Number[0] == 'Z')
                {
                    Number = "AA";
                }
                else
                {
                    StringBuilder sb = new StringBuilder(Number);
                    sb[0]++;
                    Number = sb.ToString();
                }
            }
            else
            {
                if (Number[1] == 'Z')
                {
                    StringBuilder sb = new StringBuilder(Number);
                    sb[0]++;
                    sb[1] = 'A';
                    Number = sb.ToString();
                }
                else
                {
                    StringBuilder sb = new StringBuilder(Number);
                    sb[1]++;
                    Number = sb.ToString();
                }
            }
        }
        if(NumConverted == true){
            return NumberHolder.ToString("F2") + " " + Number;
        }else    return Number;
    }
    public void print(string Number)
    {
        Debug.Log(Number);
    }
}
