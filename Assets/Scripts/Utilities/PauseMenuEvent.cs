using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
public class PauseMenuEvent : MonoBehaviour
{
    public AudioSource AudioSource;
    public GameOffBonus Bonus;
    [SerializeField] Slider VolumeSlider;
    private float MusicVolume;
    public void ReturnMainMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        Bonus.SetLogOutDT(DateTime.Now);
    }
    public void QuitGame()
    {
        Debug.Log("Quit Game");
        Application.Quit();
    }
    public void ChangeVolume()
    {
        MusicVolume = VolumeSlider.value;
        AudioSource.volume = VolumeSlider.value;
    }
}
