using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinimizeBottomMenu : MonoBehaviour
{
    public GameObject BotomMenu;
    public float MinimizeRate = 4;
    private RectTransform BotomMenuRT; 
    private bool Minimized=true;
    private Vector2 rectSize;
    private float offset;
    // Start is called before the first frame update
    void Start()
    {
        BotomMenuRT=BotomMenu.GetComponent<RectTransform>();
        rectSize = BotomMenuRT.sizeDelta;
        Vector2 TargetNewPos = BotomMenu.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition;
        offset= TargetNewPos.y - rectSize.y/2;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MinimizeMenu(){
        if(Minimized==true) return;
        rectSize = new Vector2 (rectSize.x, rectSize.y/MinimizeRate);
        
        ReducePosition(BotomMenu.transform.GetChild (0) , MinimizeRate);
        ReducePosition(BotomMenu.transform.GetChild (1) , MinimizeRate);
        // ReducePosition(BotomMenu.transform.GetChild (2) , MinimizeRate);

        BotomMenuRT.sizeDelta = rectSize;
        Minimized= true;
    }

    public void MaximizeMenu(){
        if(Minimized==false) return;
        rectSize = new Vector2 (rectSize.x, rectSize.y*MinimizeRate);

        IncreasePosition(BotomMenu.transform.GetChild (0) , MinimizeRate);
        IncreasePosition(BotomMenu.transform.GetChild (1) , MinimizeRate);
        // IncreasePosition(BotomMenu.transform.GetChild (2) , MinimizeRate);

        BotomMenuRT.sizeDelta = rectSize;
        Minimized= false;
    }

    private void ReducePosition(Transform target, float rate ){     
        Vector2 TargetNewPos = target.GetComponent<RectTransform>().anchoredPosition;
        TargetNewPos  = new Vector2(TargetNewPos.x,(TargetNewPos.y-offset)/rate+offset);
        target.GetComponent<RectTransform>().anchoredPosition = TargetNewPos;
    }

    private void IncreasePosition(Transform target, float rate ){
        Vector2 TargetNewPos = target.GetComponent<RectTransform>().anchoredPosition;
        TargetNewPos  = new Vector2(TargetNewPos.x,(TargetNewPos.y-offset)*rate+offset);
        target.GetComponent<RectTransform>().anchoredPosition = TargetNewPos;
    }
}
