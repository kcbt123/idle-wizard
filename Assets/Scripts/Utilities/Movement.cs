using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Movement : MonoBehaviour
{
    public Transform MovedObject;
    public float MovePos=4;
    public float MovementDuration=5;
    public Ease animEse;
    // Start is called before the first frame update
    void Start()
    {
        MovedObject
        .transform.DOMoveY(MovePos, 5)
        .SetEase (animEse);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
