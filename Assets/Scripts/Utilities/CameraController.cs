using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEditor;
public class CameraController : MonoBehaviour
{
    public Transform target;
    public Transform bg1;
    public Transform bg2;
    public GameOffBonus Bonus;
    public Text NotiMes; 
    private float size;
    // Start is called before the first frame update
    void Start()
    {
        size = bg1.GetComponent<BoxCollider2D>().size.y;
        //Caculate bonus after login game
        // Debug.Log("Application starting time " + DateTime.Now);
        // Debug.Log("Before fixed time " +Bonus.LogOutDT);
        // Debug.Log(Bonus.FixedLogOutSec);
        if(!Bonus.InGame){
            Bonus.InGame =true;
            Bonus.LogOutDT = Bonus.LogOutDT.AddSeconds(Bonus.FixedLogOutSec) ;
        }
        // Debug.Log("After fixed  time " +Bonus.LogOutDT);
        Bonus.CaculateLogOutBonus(DateTime.Now);
        List<string> NoteList = Bonus.NotificationMessage();
        NotiMes.text="";
        for (int i = 0; i < NoteList.Count; i++)
        {
            NotiMes.text += NoteList[i];
            NotiMes.text +="\n";
        }
        
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 targetPos = new Vector3(target.position.x, target.position.y, transform.position.z);
        transform.position = targetPos;
        if (transform.position.y > bg2.position.y)
        {
            bg1.position = new Vector3(bg2.position.x, bg2.position.y + size, bg1.position.z);
            SwitchBackGround();
        }
    }
    public void SwitchBackGround()
    {
        Transform temp = bg1;
        bg1 = bg2;
        bg2 = temp;
    }
    void OnApplicationQuit()
    {
        // Debug.Log("Application ending time " + DateTime.Now);
        Bonus.SetLogOutDT(DateTime.Now);
        Bonus.CalculateFixedLO();
        Bonus.InGame=false;
    }
}
