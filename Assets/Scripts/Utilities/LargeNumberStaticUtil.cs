using UnityEngine;
using System.Text;
using System;
public class LargeNumberStaticUtil
{
    public static string Addition(string Number, string AddedNumber)
    {
        if (!CompareNumber(Number, AddedNumber))
        {
            return Addition(AddedNumber, Number);
        }
        float fNumber = 0;
        float fAddedNumber = 0;
        ExtractStringNumber(ref Number, ref fNumber);
        ExtractStringNumber(ref AddedNumber, ref fAddedNumber);
        Debug.Log(Number);
        Debug.Log(AddedNumber);
        if (Number == AddedNumber)
        {
            Debug.Log(1);
            fNumber += fAddedNumber;
            while (fNumber > 1000)
            {
                fNumber = fNumber / 1000;
                if (Number == "") return fNumber.ToString("F2") + "a";
                if (Number.Length == 1)
                {
                    if (Number[0] == 'z')
                    {
                        Number = "aa";
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(Number);
                        sb[0]++;
                        Number = sb.ToString();
                    }
                }
                else
                {
                    if (Number[1] == 'z')
                    {
                        StringBuilder sb = new StringBuilder(Number);
                        sb[0]++;
                        sb[1] = 'a';
                        Number = sb.ToString();
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(Number);
                        sb[1]++;
                        Number = sb.ToString();
                    }
                }
            }
        }
        return fNumber.ToString("F2") + Number;
    }

    public static string Subtraction(string Number, string SubtractedNumber)
    {
        if (!CompareNumber(Number, SubtractedNumber))
        {
            return "";
        }
        float fNumber = 0;
        float fSubtractedNumber = 0;
        ExtractStringNumber(ref Number, ref fNumber);
        ExtractStringNumber(ref SubtractedNumber, ref fSubtractedNumber);
        if (Number == SubtractedNumber)
        {
            fNumber -= fSubtractedNumber;
            if (Number.Length == 1)
            {
                if (fNumber < 1)
                {
                    fNumber = fNumber * 1000;
                    if (Number[0] == 'a')
                    {
                        Number = "";
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(Number);
                        sb[0]--;
                        Number = sb.ToString();
                    }
                }
            }
            if (Number.Length == 2)
            {
                if (fNumber < 1)
                {
                    fNumber = fNumber * 1000;
                    if (Number[1] == 'a')
                    {
                        if (Number[0] == 'a')
                        {
                            Number = "z";
                        }
                        else
                        {
                            StringBuilder sb = new StringBuilder(Number);
                            sb[0]--;
                            sb[1] = 'z';
                            Number = sb.ToString();
                        }
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(Number);
                        sb[1]--;
                        Number = sb.ToString();
                    }
                }
            }

        }
        return fNumber.ToString("F2") + Number;
    }

    public static string Multiplication(string Number, float Multiplier)
    {
        float fNumber = 0;
        ExtractStringNumber(ref Number, ref fNumber);
        fNumber *= Multiplier;
        while (fNumber > 1000)
        {
            fNumber = fNumber / 1000;
            if (Number == "")
            {
                Number = "a";
                continue;
            }
            if (Number.Length == 1)
            {
                if (Number[0] == 'z')
                {
                    Number = "aa";
                }
                else
                {
                    StringBuilder sb = new StringBuilder(Number);
                    sb[0]++;
                    Number = sb.ToString();
                }
            }
            else
            {
                if (Number[1] == 'z')
                {
                    StringBuilder sb = new StringBuilder(Number);
                    sb[0]++;
                    sb[1] = 'a';
                    Number = sb.ToString();
                }
                else
                {
                    StringBuilder sb = new StringBuilder(Number);
                    sb[1]++;
                    Number = sb.ToString();
                }
            }
        }
        return fNumber.ToString("F2") + Number;
    }

    public static bool CompareNumber(string Number, string ComparedNumber)
    {
        float fNumber = 0;
        float fComparedNumber = 0;
        ExtractStringNumber(ref Number, ref fNumber);
        ExtractStringNumber(ref ComparedNumber, ref fComparedNumber);
        if (Number == "" && ComparedNumber == "")
        {
            if (fNumber > fComparedNumber || fNumber == fComparedNumber) { return true; } else return false;
        }
        if (Number.Length > ComparedNumber.Length)
        {
            return true;
        }
        if (Number.Length == ComparedNumber.Length)
        {
            if (Number[0] > ComparedNumber[0])
            {
                return true;
            }
            if (Number[0] == ComparedNumber[0])
            {
                if (Number.Length == 2)
                {
                    if (Number[1] > ComparedNumber[1]) return true;
                    if (Number[1] < ComparedNumber[1]) return false;
                }
                if (fNumber > fComparedNumber || fNumber == fComparedNumber) { return true; } else return false;
            }
            else return false;
        }
        else { return false; }
    }

    public static void ExtractStringNumber(ref string Number, ref float fNumber)
    {
        for (int i = Number.Length - 1; i > 0; i--)
        {
            if (!Char.IsDigit(Number, i))
            {
                continue;
            }
            else
            {
                fNumber = float.Parse(Number.Substring(0, i + 1));
                if (i == (Number.Length - 1))
                {
                    Number = "";
                }
                else Number = Number.Substring(i + 2);
                break;
            }
        }
    }

    public static string ConvertNumber(string NumberConvert)
    {
        string Number = NumberConvert;
        bool NumConverted = false;
        float NumberHolder = 0;
        ExtractStringNumber(ref Number, ref NumberHolder);
        while (NumberHolder > 10)
        {
            if (Number == "zz")
            {
                Debug.Log("Max number can't convert anymore");
                break;
            }
            NumConverted = true;
            NumberHolder /= 1000;
            if (Number == "")
            {
                Number = "a";
                continue;
            }
            if (Number.Length == 1)
            {
                if (Number[0] == 'z')
                {
                    Number = "aa";
                }
                else
                {
                    StringBuilder sb = new StringBuilder(Number);
                    sb[0]++;
                    Number = sb.ToString();
                }
            }
            else
            {
                if (Number[1] == 'z')
                {
                    StringBuilder sb = new StringBuilder(Number);
                    sb[0]++;
                    sb[1] = 'a';
                    Number = sb.ToString();
                }
                else
                {
                    StringBuilder sb = new StringBuilder(Number);
                    sb[1]++;
                    Number = sb.ToString();
                }
            }
        }

        if (NumConverted == true)
        {
            return NumberHolder.ToString("F2") + Number;
        }
        else return Number;
    }
}
