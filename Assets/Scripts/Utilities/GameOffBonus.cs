using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[CreateAssetMenu(fileName = "GameOffBonus", menuName = "idle-wizard/GameOffBonus", order = 0)]
public class GameOffBonus : ScriptableObject
{
    public DateTime LogOutDT = new DateTime(2021, 8, 19, 0, 0, 0, DateTimeKind.Utc);
    public int FixedLogOutSec;
    private TimeSpan result;
    private int BossCount;
    private int LogOutTotalSec;
    private string GoldBonus="";
    public bool InGame = false;
    public void SetLogOutDT(DateTime LogOut)
    {
        this.LogOutDT = LogOut;
        // Debug.Log("SetLogOutDT :"  +LogOutDT);
    }

    public List<string> NotificationMessage (){
         List<string> NoteList = new List<string>();
         string TimeCountMes = "Time passed: "+ result.Days+ " Days " + result.Hours+ " Hours " + result.Minutes+ " Minutes " +result.Seconds+ " Seconds";
         string BossKilledMes = "Kill total: "+ BossCount+" Bosses";
         string GoldBonusMes = "Gold gained: "+ GoldBonus;

         NoteList.Add(TimeCountMes);
         NoteList.Add(BossKilledMes);
         NoteList.Add(GoldBonusMes);
        return NoteList;
    }

    public void CaculateLogOutBonus(DateTime LogIn){
        LogOutTotalSec = CalculateLogOutSecs(LogIn);
        CaculateGoldBonus();
    }
        public int CalculateLogOutSecs(DateTime LogIn)
    {
        result  = LogIn.Subtract(LogOutDT);
        int seconds = Convert.ToInt32(result.TotalSeconds);
        return seconds;
    }
    //Caculate gold gained and total boss killed
    public void CaculateGoldBonus(){
        
    }

    public void CalculateFixedLO(){
        result  = DateTime.Now.Subtract(new DateTime(2021, 8, 19, 0, 0, 0, DateTimeKind.Utc));
        FixedLogOutSec = Convert.ToInt32(result.TotalSeconds);
        // Debug.Log("Fixed Sec: "+FixedLogOutSec);
    }
}